module Benchmark.Domain

open System

type Date =
  | Inception of DateTime
  | Intermediate of DateTime
  | Present

type DateDuration =
  { StartDate : Date
    EndDate : Date    
  }

type Duration =
  { Start : DateTime 
    End : DateTime
  }

type SanitizedDuration =
  | ValidDuration of Duration
  | InvalidDuration of Duration

type Interval =
  | Daily of SanitizedDuration
  | Monthly of SanitizedDuration
  | Quarterly of SanitizedDuration
  | InvalidInterval of SanitizedDuration

let unwrap (sanitizedDuration: SanitizedDuration) =
  match sanitizedDuration with
      | ValidDuration(x) -> x
      | InvalidDuration(x) -> x

type Period =
  | Partial of Interval
  | Full of Interval
  | InvalidPeriod of Interval


type Return = decimal

type WeightedReturn =
  { Weight: decimal
    Return: Return
  }
  member this.CalculatedReturn = this.Weight * this.Return

type Methodology =
  | PreWall
  | PostWall

type PeriodReturn = Period * WeightedReturn


module Period =

  let isFullPeriod isSamePeriod isFirstDayOfPeriod isLastDayOfPeriod (startDate : DateTime) (endDate : DateTime) =
    match startDate,endDate with
    | x,y when isSamePeriod x y ->
      match x,y with
      | x,y when isFirstDayOfPeriod x && isLastDayOfPeriod y -> true
      | _,_ -> false
    | _,_ -> false

  let isSameDay (startDate : DateTime) (endDate : DateTime) =
    startDate = endDate

  let isSameMonth (startDate : DateTime) (endDate : DateTime) =
    startDate.Year = endDate.Year && startDate.Month = endDate.Month

  let getFirstDayOfMonth (date: DateTime) =
    DateTime(date.Year, date.Month, 1)

  let isFirstDayOfMonth (getFirstDayOfMonth: DateTime -> DateTime) (date: DateTime) =
    date = getFirstDayOfMonth date

  let getLastDayOfMonth (date: DateTime) =
    DateTime(date.Year, date.Month + 1, 1).AddDays(- 1.)

  let isLastDayOfMonth (getLastDayOfMonth: DateTime -> DateTime) (date: DateTime) =
    date = getLastDayOfMonth date

  let getQuarter (date: DateTime) =
    Math.Abs((date.Month - 1) / 3) + 1

  let addQuarters (date: DateTime) quarters =
    date.AddMonths(quarters * 3)

  let getFirstDayOfQuarter (addQuarters: DateTime -> int -> DateTime) (getQuarter: DateTime -> int) (date: DateTime) =    
    addQuarters (DateTime(date.Year,1,1)) ((getQuarter date) - 1)

  let getLastDayOfQuarter (addQuarters: DateTime -> int -> DateTime) (getQuarter: DateTime -> int) (date: DateTime) =
    let firstDayNextQuarter = addQuarters (DateTime(date.Year,1,1)) (getQuarter date)
    firstDayNextQuarter.AddDays(- 1.)

  let isSameQuarter (getQuarter: DateTime -> int) startDate endDate =
    (getQuarter startDate) = (getQuarter endDate)

  let isFirstDayOfQuarter (getFirstDayOfQuarter: DateTime -> DateTime) (date: DateTime) =
    date = getFirstDayOfQuarter date

  let isLastDayOfQuarter (getLastDayOfQuarter: DateTime -> DateTime) (date: DateTime) =
    date = getLastDayOfQuarter date