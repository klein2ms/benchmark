module Benchmark.Tests

open Expecto
open Swensen.Unquote
open Calculators
open Domain
open Wranglers
open System

[<Tests>]
let tests =
  testList "Tests" [
    testList "Calulators" [
      testCase "calculate returns by period returns correct result" <| fun _ ->

        let firstPeriod =
          Partial(Monthly(ValidDuration({ Start=DateTime(2018,1,19); End=DateTime(2018,1,31)})))
        
        let secondPeriod =
          Full(Monthly(ValidDuration({ Start=DateTime(2018,2,1); End=DateTime(2018,2,28)})))

        let thirdPeriod =
          Full(Monthly(ValidDuration({ Start=DateTime(2018,3,1); End=DateTime(2018,3,31)}))) 

        let returns = 
          [ (firstPeriod, { Weight=0.25M; Return=0.01M })
            (firstPeriod, { Weight=0.25M; Return=0.0125M })
            (firstPeriod, { Weight=0.25M; Return=0.015M })
            (firstPeriod, { Weight=0.25M; Return=0.0175M })        
            (secondPeriod, { Weight=0.5M; Return=0.02M })
            (secondPeriod, { Weight=0.5M; Return=0.025M })          
            (thirdPeriod, { Weight=0.125M; Return=0.03M })
            (thirdPeriod, { Weight=0.125M; Return=0.0325M })
            (thirdPeriod, { Weight=0.25M; Return=0.035M })
            (thirdPeriod, { Weight=0.5M; Return=0.0375M })          
          ]
        
        let calculator = calculateWeightedReturns calculateWeightedReturn
        let actual =
          calculateReturnsBy<Period> aggregateReturnsBy<Period> calculator returns
        
        let expected =
          [ (firstPeriod, 0.01375M)
            (secondPeriod, 0.0225M)
            (thirdPeriod, 0.0353125M)                  
          ]

        test <@ actual |> List.ofSeq = expected @>
      
      testCase "linkedReturn returns correct result" <| fun _ ->
        let returns =
          [ 0.065M
            0.055M         
            0.022M
          ]
        
        let actual = linkedReturn calculateLinkedReturn returns

        let expected = 0.14829365M

        test <@ actual = expected @>
      
      testCase "calculateDerivedConstantReturn returns correct result" <| fun _ ->
        let actual = calculateDerivedConstantReturn 31. 0.09M

        let expected = 0.00278379326163081M

        test <@ actual = expected @>

      testCase "calculatePeriodReturn returns correct result" <| fun _ ->        
        let derivedReturnCalculator = calculateDerivedConstantReturn        
        let linkedReturnCalculator = linkedReturn calculateLinkedReturn
        let calculatePeriodReturn = calculatePeriodReturn durationCalculator derivedReturnCalculator linkedReturnCalculator

        let period = Partial(Monthly(ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,1,15)})))
        let weightedReturn = { Weight=0.25M; Return=0.09M }

        let periodReturn = period,weightedReturn
        
        let actual = calculatePeriodReturn periodReturn

        let expected = period,Some({ Weight=0.25M; Return=0.0425804951445599804348131131M })

        test <@ actual = expected @>
    ]

    testList "Wranglers" [
      testCase "toPeriod with full monthly interval returns correct result" <| fun _ ->
        
        let validDuration = ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,1,31) })
        let interval = Monthly(validDuration)
        
        let daily _ interval = InvalidPeriod(interval)
        let isSameMonth _ _ = true
        let isFullMonth _ _ = true
        let monthly = monthlyDurationToPeriod isSameMonth isFullMonth
        let quarterly _ interval = InvalidPeriod(interval)

        let actual = toPeriod daily monthly quarterly interval

        let expected = Full(interval)

        test <@ actual = expected @>
      
      testCase "toPeriod with full quarterly interval returns correct result" <| fun _ ->
        
        let validDuration = ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,3,31) })
        let interval = Quarterly(validDuration)
        
        let daily _ interval = InvalidPeriod(interval)        
        let monthly _ interval = InvalidPeriod(interval)

        let isSameQuarter _ _ = true
        let isFullQuarter _ _ = true
        
        let quarterly = quarterlyDurationToPeriod isSameQuarter isFullQuarter

        let actual = toPeriod daily monthly quarterly interval

        let expected = Full(interval)

        test <@ actual = expected @>
      
      testCase "integration toPeriod with full quarterly interval returns correct result" <| fun _ ->
        
        let validDuration = ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,3,31) })
        let interval = Quarterly(validDuration)
        
        let daily _ interval = InvalidPeriod(interval)        
        let monthly _ interval = InvalidPeriod(interval)

        let isSameQuarter = Period.isSameQuarter Period.getQuarter
        let getFirstDayOfQuarter = Period.getFirstDayOfQuarter Period.addQuarters Period.getQuarter
        let isFirstDayOfQuarter = Period.isFirstDayOfQuarter getFirstDayOfQuarter

        let getLastDayOfQuarter = Period.getLastDayOfQuarter Period.addQuarters Period.getQuarter
        let isLastDayOfQuarter = Period.isLastDayOfQuarter getLastDayOfQuarter

        let isFullQuarter = Period.isFullPeriod isSameQuarter isFirstDayOfQuarter isLastDayOfQuarter
        
        let quarterly = quarterlyDurationToPeriod isSameQuarter isFullQuarter

        let actual = toPeriod daily monthly quarterly interval

        let expected = Full(interval)

        test <@ actual = expected @>
      
      testCase "wrangle returns correct result" <| fun _ ->
        let duration = 
          { StartDate = Inception(DateTime(2018,1,1))
            EndDate = Present            
          }
        
        let dateConverter (date : Date) =
          match date with
          | Inception _ -> DateTime(2018,1,1)
          | Intermediate x -> x
          | Present -> DateTime(2018,1,19)
        
        let durationConverter = toDuration dateConverter

        let isSanitized _ = true
        let sanitizeDuration = sanitizeDuration isSanitized

        let intervalConverter (sanitizedDuration : SanitizedDuration) =
          Monthly(sanitizedDuration)

        let daily _ interval = InvalidPeriod(interval)
        let isSameMonth _ _ = true
        let isFullMonth _ _ = false
        let monthly = monthlyDurationToPeriod isSameMonth isFullMonth
        let quarterly _ interval = InvalidPeriod(interval)

        let periodConverter = toPeriod daily monthly quarterly

        let wrangle = wrangle durationConverter sanitizeDuration intervalConverter periodConverter

        let actual = wrangle duration

        let expected = 
          Partial(Monthly(ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,1,19)})))
        
        test <@ actual = expected @>
      
      testCase "lagDuration given day offset returns correct result" <| fun _ ->
        let duration = ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,1,31) })
        let actual = lagDuration dayOffset -2 duration

        let expected = ValidDuration({ Start=DateTime(2017,12,30); End=DateTime(2018,1,29) })

        test <@ actual = expected @>
      
      testCase "lagDuration given month offset returns correct result" <| fun _ ->
        let duration = ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,1,31) })
        let actual = lagDuration monthOffset -2 duration

        let expected = ValidDuration({ Start=DateTime(2017,11,1); End=DateTime(2017,11,30) })

        test <@ actual = expected @>
      
      testCase "lagDuration given quarterly offset returns correct result" <| fun _ ->
        let duration = ValidDuration({ Start=DateTime(2018,1,1); End=DateTime(2018,4,30) })
        let actual = lagDuration quarterOffset -2 duration

        let expected = ValidDuration({ Start=DateTime(2017,7,1); End=DateTime(2017,10,30) })

        test <@ actual = expected @>

      testCase "lagInterval given monthly interval returns correct result" <| fun _ ->
        let interval = Monthly(ValidDuration({ Start=DateTime(2018,1,15); End=DateTime(2018,1,31) }))
        
        let dailyOffset _ x = x
        let quarterlyOffset _ x = x
        let monthlyOffset = lagDuration monthOffset
        let lagInterval = lagInterval dailyOffset monthlyOffset quarterlyOffset
        let actual = lagInterval -2 interval

        let expected = Monthly(ValidDuration({ Start=DateTime(2017,11,15); End=DateTime(2017,11,30) }))

        test <@ actual = expected @>

      testCase "addRiskPremium returns correct result" <| fun _ ->
        let annualizeReturn = annualizeReturn 12.
        let deannualizeReturn = deannualizeReturn 12.
        let addRiskPremiumTo = addRiskPremium annualizeReturn deannualizeReturn 0.02M

        let actual = addRiskPremiumTo 0.00044M
        let expected = 0.00208368676295101M

        test <@ actual = expected @>

      testCase "addRiskPremium of 0 returns correct result" <| fun _ ->
        let annualizeReturn = annualizeReturn 12.
        let deannualizeReturn = deannualizeReturn 12.
        let addRiskPremiumTo = addRiskPremium annualizeReturn deannualizeReturn 0.00M

        let actual = addRiskPremiumTo 0.02M
        let expected = 0.02M

        test <@ actual = expected @>

      testCase "missingPeriods returns correct result" <| fun _ ->
        let durations =
          [ { Start=DateTime(2017,1,15); End=DateTime(2017,6,30) }
            { Start=DateTime(2017,8,1); End=DateTime(2017,9,30) }
            { Start=DateTime(2017,11,1); End=DateTime(2017,12,25) }
            { Start=DateTime(2017,12,30); End=DateTime(2017,12,30) }                        
          ]

        let startDate = DateTime(2017,1,1)
        let endDate = DateTime(2017,12,31)

        let actual = missingPeriods startDate endDate durations

        let expected = 
          [ { Start=DateTime(2017,1,1); End=DateTime(2017,1,14) }            
            { Start=DateTime(2017,7,1); End=DateTime(2017,7,31) }
            { Start=DateTime(2017,10,1); End=DateTime(2017,10,31) }
            { Start=DateTime(2017,12,26); End=DateTime(2017,12,29) }
            { Start=DateTime(2017,12,31); End=DateTime(2017,12,31) }                                                                                    
          ]

        test <@ actual = expected @>

      testCase "overlappingPeriods returns correct result" <| fun _ ->
        let durations =
          [ { Start=DateTime(2017,1,15); End=DateTime(2017,6,30) }
            { Start=DateTime(2017,1,31); End=DateTime(2017,5,15) }            
            { Start=DateTime(2017,6,15); End=DateTime(2017,7,31) }
            { Start=DateTime(2017,8,1); End=DateTime(2017,8,9) }                                                
            { Start=DateTime(2017,8,10); End=DateTime(2017,8,16) }                                    
            { Start=DateTime(2017,8,16); End=DateTime(2017,8,30) }                        
          ]

        let actual = overlappingPeriods durations

        let expected =
          [ { Start=DateTime(2017,1,31); End=DateTime(2017,5,15) } 
            { Start=DateTime(2017,6,15); End=DateTime(2017,6,30) }
            { Start=DateTime(2017,8,16); End=DateTime(2017,8,16) }                                                                                 
          ]

        test <@ actual = expected @>        
    ]            
  ]