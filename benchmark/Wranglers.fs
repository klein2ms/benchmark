module Benchmark.Wranglers

open System
open Domain

type DateTimeConverter = Date -> DateTime
type DurationConverter = DateDuration -> Duration
type DurationSanitizer = Duration -> SanitizedDuration
type IntervalConverter = SanitizedDuration -> Interval
type PeriodConverter = Interval -> Period

type PeriodWrangler = DateDuration -> Period

let wrangle (durationConverter : DurationConverter) (durationSanitizer : DurationSanitizer) (intervalConverter : IntervalConverter) (periodConverter : PeriodConverter) duration =
  (durationConverter 
  >> durationSanitizer 
  >> intervalConverter 
  >> periodConverter) duration

let toDateTime  (getPresent : unit -> DateTime) (getInception : string -> DateTime) date =
  match date with
  | Inception x -> x
  | Intermediate x -> x
  | Present -> getPresent ()

let toDuration (convertor : DateTimeConverter) (dateDuration : DateDuration) =
  { Start = (convertor dateDuration.StartDate)
    End = (convertor dateDuration.EndDate)
  }

let isValidDuration (duration: Duration) =
  match duration with
  | duration when duration.Start <= duration.End -> true
  | _ -> false
  
let sanitizeDuration isValidDuration (duration: Duration) =
  match duration with
  | duration when isValidDuration duration -> ValidDuration(duration)
  | _ -> InvalidDuration(duration)

let toInterval (convertor : IntervalConverter) (duration: SanitizedDuration) =
  convertor duration

let dailyDurationToPeriod isSameDay (duration: SanitizedDuration) (interval: Interval) =
  match duration with
  | ValidDuration x when isSameDay x.Start x.End -> Full(interval)  
  | _ -> InvalidPeriod(interval)

let monthlyDurationToPeriod isSameMonth isFullMonth (duration: SanitizedDuration) (interval: Interval) =
  match duration with
  | ValidDuration x when isSameMonth x.Start x.End ->
    match isFullMonth x.Start x.End with
    | true -> Full(interval)
    | false -> Partial(interval)
  | _ -> InvalidPeriod(interval) 

let quarterlyDurationToPeriod isSameQuarter isFullQuarter (duration: SanitizedDuration) (interval: Interval) =
  match duration with  
  | ValidDuration x when isSameQuarter x.Start x.End ->
    match isFullQuarter x.Start x.End with
    | true -> Full(interval)
    | false -> Partial(interval)
  | _ -> InvalidPeriod(interval)

let toPeriod dailyDurationToPeriod monthlyDurationToPeriod quarterlyDurationToPeriod (interval: Interval) =
  match interval with
  | Daily x -> dailyDurationToPeriod x interval
  | Monthly x -> monthlyDurationToPeriod x interval
  | Quarterly x -> quarterlyDurationToPeriod x interval
  | _ -> InvalidPeriod(interval)

type Periodicity =
  | Day
  | Month
  | Quarter
  | Year

let offsetDate periodicity offset (date : DateTime) =
  match periodicity with
  | Day -> date.AddDays(float offset)
  | Month -> date.AddMonths(offset)
  | Quarter -> Period.addQuarters date offset
  | Year -> date.AddYears(offset)

type DateOffset = int -> DateTime -> DateTime

let dayOffset = offsetDate Day
let monthOffset = offsetDate Month
let quarterOffset = offsetDate Quarter
let yearOffset = offsetDate Year

let lagDuration (f : DateOffset) offset (duration: SanitizedDuration) =
  match duration with
  | ValidDuration x -> 
    let applyOffset = f offset
    ValidDuration({ Start = applyOffset x.Start; End = applyOffset x.End })
  | _ -> duration

let lagInterval dailyOffset monthlyOffset quarterlyOffset offset (interval : Interval) =
  match interval with
  | Daily x -> Daily(dailyOffset offset x)
  | Monthly x -> Monthly(monthlyOffset offset x)
  | Quarterly x -> Quarterly(quarterlyOffset offset x)
  | _ -> interval

type InvalidTimeSeries =
  | MissingTimePeriods of Period list
  | OverlappingTimePeriods of Period list

type TimeSeries =
  | ValidTimeSeries of Period list
  | InvalidTimeSeries of InvalidTimeSeries list

let missingPeriods (startDate : DateTime) (endDate : DateTime) (durations : Duration list) =
  match durations with
  | [] -> [{ Start=startDate; End=endDate }]
  | _::_ ->
      let missingStart =
        let firstDate  = 
          List.minBy (fun x -> x.Start,x.End) durations
        match firstDate with
        | firstDate when firstDate.Start <> startDate -> [{Start=startDate; End=firstDate.Start.AddDays(-1.) }]
        | _ -> []
      
      let missingEnd =
        let lastDate =
          List.maxBy (fun x -> x.Start,x.End) durations
        match lastDate with
        | lastDate when lastDate.End <> endDate -> [{Start=lastDate.End.AddDays(1.); End=endDate}]
        | _ -> []
      
      let missingIntermediates =
        durations
        |> List.sortBy (fun x -> x.Start,x.End)
        |> List.pairwise
        |> List.filter (fun (x,y) -> x.End.AddDays(1.) <> y.Start)
        |> List.map(fun (x,y) -> {Start=x.End.AddDays(1.); End=y.Start.AddDays(-1.)})  

      missingStart @ missingIntermediates @ missingEnd

let cart2 xs ys = 
  xs 
  |> List.collect (fun x -> ys |> List.map (fun y -> x, y))

let selfJoin xs = cart2 xs xs

let overlappingPeriods (durations: Duration list) =
  match durations with
  | [] -> []
  | _::_ ->    
    durations 
    |> selfJoin
    |> List.filter (fun (x,y) -> x <> y)
    |> List.sortBy (fun (x,y) -> x.Start, x.End, y.Start, y.End)
    |> List.collect (fun (x,y) ->
      match x,y with
      | x,y when x.Start < y.Start && x.End > y.End -> [y]
      | x,y when x.Start < y.Start && x.End > y.Start -> [{ Start=y.Start; End=x.End }]
      | x,y when x.End = y.Start -> [{ Start=x.End; End=y.Start }]
      | _,_ -> []
    )

type Proxy =
  | ZeroPerformance
  | Rolling12MonthAveragePerforamance
