module Benchmark.Calculators

open Domain

type WeightedReturnCalculator = WeightedReturn -> Return
type TimeWeightedReturnCalculator = (Date * WeightedReturn) list -> Date * Return
type Aggregator<'a> = ('a * WeightedReturn) list -> ('a * (WeightedReturn list)) list

let calculateWeightedReturn (r : WeightedReturn) =
  r.Weight * r.Return

let calculateWeightedReturns (calculator : WeightedReturnCalculator) (returns : WeightedReturn list) =
  List.sumBy calculator returns

let aggregateReturnsBy<'a when 'a : equality> (returns : ('a * WeightedReturn) list) =
  returns
  |> List.groupBy (fst)
  |> List.map (fun (x,y) -> 
    x,
    y |> List.map (snd)
  )

let calculateReturnsBy<'a when 'a : equality> (aggregator : Aggregator<'a>) (calculator : WeightedReturn list -> decimal) (returns : ('a * WeightedReturn) list) =
  returns
  |> aggregator
  |> List.map (fun (x,y) ->
    x,
    y |> calculator
  )

let linkedReturn (calculator : 'a list -> 'a) (returns : 'a list) =
  returns |> calculator

let calculateLinkedReturn (returns : decimal list) =
  List.fold (fun acc elem -> acc * (1M + elem)) 1M returns
  |> function x -> (x - 1M)

let calculateDerivedConstantReturn numberOfSubperiods (periodReturn : decimal) =
  ((float periodReturn) + 1.) ** (1. / numberOfSubperiods) - 1.
  |> decimal

let durationCalculator (interval : Interval) =
  match interval with
  | Daily _ -> Some(1.,1.)
  | Monthly x -> 
    let duration = unwrap x
    let totalNumberOfSubperiodsInInterval = (Period.getLastDayOfMonth duration.End - Period.getFirstDayOfMonth duration.Start).TotalDays + 1.
    let numberOfSubperiodsInDuration = (duration.End - duration.Start).TotalDays + 1.
    Some(totalNumberOfSubperiodsInInterval, numberOfSubperiodsInDuration)
  | Quarterly x ->
    let duration = unwrap x
    let getLastDayOfQuarter = Period.getLastDayOfQuarter Period.addQuarters Period.getQuarter
    let getFirstDayOfQuarter = Period.getFirstDayOfQuarter Period.addQuarters Period.getQuarter
    let totalNumberOfSubperiodsInInterval = (getLastDayOfQuarter duration.End - getFirstDayOfQuarter duration.Start).TotalDays + 1.
    let numberOfSubperiodsInDuration = (duration.End - duration.Start).TotalDays + 1.
    Some(totalNumberOfSubperiodsInInterval, numberOfSubperiodsInDuration)
  | InvalidInterval _ -> None

let calculatePeriodReturn (durationCalculator : Interval -> (float*float) option ) derivedConstantReturnCalculator linkedReturnCalculator (periodReturn : PeriodReturn) =
  match periodReturn with
  | InvalidPeriod _,_ -> (fst periodReturn, None)
  | Partial x,y ->    
    let totalNumberOfSubperiods,numberOfSubperiods = Option.get <| durationCalculator x
    let derivedConstantReturn = derivedConstantReturnCalculator totalNumberOfSubperiods y.Return              
    let partialPeriodReturn = linkedReturnCalculator [ for _ in 1 .. int numberOfSubperiods -> derivedConstantReturn ]    
    (fst periodReturn, Some({ y with Return = partialPeriodReturn }))  
  | Full _,y ->
    (fst periodReturn, Some(y))

let annualizeReturn numberOfPeriods (performanceReturn : Return) =  
  (float performanceReturn + 1.) ** numberOfPeriods - 1. 
  |> decimal

let deannualizeReturn numberOfPeriods (performanceReturn : Return) =
  (float performanceReturn + 1.) ** (1. / numberOfPeriods) - 1.
  |> decimal

let addRiskPremium annualizeReturn deannualizeReturn (premium : decimal) (performanceReturn : Return) =
  (annualizeReturn
  >> (fun x -> x + premium)
  >> deannualizeReturn) performanceReturn

type Proxy =
  | Previous12MonthAverageReturn
  | ZeroReturn

let proxyReturn (proxy : Proxy) (performanceReturns : Return list) =
  match proxy with
  | Previous12MonthAverageReturn -> performanceReturns |> List.average |> Some
  | ZeroReturn -> Some(0.0M)


